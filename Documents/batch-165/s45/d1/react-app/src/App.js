import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {UserProvider} from './UserContext';
import './App.css';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Courses from './pages/Courses'
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/Page404';

import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {    
  
  const [login, setLogin] = useState('');
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  });
  const unsetUser = () => {
    localStorage.clear();
    setUser({email: null});
  }
  return (
    <UserProvider value={{user, setUser, unsetUser}} >
    <Router>
     <AppNavbar login={login} />
      <Container>
        <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="/courses" element={ <Courses/> } />
          <Route path="/register" element={ <Register/> } />
          <Route path="/login" element={ <Login setLogin={setLogin}/> } />
          <Route path="/logout" element={ <Logout setLogin={setLogin}/> } />
          <Route path='*' element={ <Page404/> } />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
