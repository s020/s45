import React, {useContext, useEffect, useState} from 'react';
import {Navigate} from 'react-router-dom';
import AppNavbar from '../components/AppNavbar';
import UserContext from '../UserContext';

export default function Logout({setLogin}) {
	localStorage.removeItem('email');
	const {unsetUser} = useContext(UserContext);
	unsetUser();
	return(
		<Navigate to="/" />
	
	);
}