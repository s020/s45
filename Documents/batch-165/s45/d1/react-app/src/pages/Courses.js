import React from 'react';
import coursesData from '../mockData/coursesData';
import CourseCard from '../components/CourseCard'


export default function Courses() {
	const courses = coursesData.map(x => {
		return <CourseCard key={x.id} {...x}/>
	})

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>

	);
}