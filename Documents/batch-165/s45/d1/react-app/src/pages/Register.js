import React, {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
export default function Register() {
	
	const [email, setEmail] = useState('');
	const [pass, setPass] = useState('');
	const [verPass, setVerPass] = useState('');
	const [isActive, setActive] = useState(true);

	useEffect(() => {
		if((email !== '' && pass !== '' && verPass !== '') && (pass === verPass))
			setActive(true);
		else
			setActive(false);
	}, [email, pass, verPass])

	function registerUser(e) {
		e.preventDefault();
		setEmail('');
		setPass('');
		setVerPass('');
		Swal.fire(
			"Success!",
			"You are now Registered",
			"success"
		)
	}

	return(
	<Form className="my-5" onSubmit={e => registerUser(e)}>
		<h1>Register</h1>
		<Form.Group>
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter Email" onChange={e => setEmail(e.target.value)} value={email} required />
			<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
		</Form.Group>

		<Form.Group>
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" onChange={e => setPass(e.target.value)} value={pass} required />
		</Form.Group>

		<Form.Group>
			<Form.Label>Verify Password:</Form.Label>
			<Form.Control type="password" placeholder="Verify Password" onChange={e => setVerPass(e.target.value)} value={verPass} required />
		</Form.Group>
		{isActive? <Button variant="primary" type="submit" className="mt-3">Submit</Button> :
		<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button> }
		

	</Form>
	);
}