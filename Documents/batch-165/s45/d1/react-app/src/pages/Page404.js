import React from 'react'
import {Image, Row, Col} from 'react-bootstrap';

export default function Page404() {
	return(<>
		<Row>
			<Image  as={Col} src="https://www.tangopixel.com/assets/images/3dcart/3dcart-error-page.jpg" />
		</Row>
	</>)
}