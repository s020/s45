import React, {useContext, useEffect, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Navigate} from 'react-router-dom';
import AppNavbar from '../components/AppNavbar';
import UserContext from '../UserContext';
export default function Login() {


	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('');
	const [pass, setPass] = useState('');
	const [active, setActive] = useState(false);
	
	useEffect(() => {
		if (email !== '' && pass !== '')
			setActive(true);
		else
			setActive(false);
	}, [email, pass]);

	function handleSubmit(e) {
		e.preventDefault();
		localStorage.setItem("email", email);
		Swal.fire("Login Successful", "", "success");

		fetch('http://localhost:3500/users/login', 
		{
			method: "POST",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify({
				email: email,
				password: pass
			})
		}).then(response => response.text()).then(data => console.log(`this is the data ${data}`));

				
	}
	return(
		<div>
			<Form>
				<h1>Login</h1>

				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control type="email" onChange={e => setEmail(e.target.value)} value={email} required />
				</Form.Group>

				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" onChange={e => setPass(e.target.value)} value={pass} required />
				</Form.Group>

				{(active) ? <Button className="bg-warning" onClick={e => handleSubmit(e)}>Submit</Button> :
				<Button className="bt-warning" disabled>Submit</Button>}

				{(user.email !== null) ? <Navigate to="/" /> : <></>}

			</Form>
		</div>
	);
}