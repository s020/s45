const coursesData = [
	{
		id: "wdc001",
		imgLink: "https://www.cloudways.com/blog/wp-content/uploads/Laravel-9.jpg",
		name: "PHP - Laravel",
		description: "A course on this programming language",
		price: 40000,
		onOffer: true
	},
	{
		id: "wdc002",
		imgLink:"https://res.cloudinary.com/practicaldev/image/fetch/s--YF5dMOu4--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/p7n9jvld8orrfyyjg4jb.jpg",
		name: "Python - Django",
		description: "A course on this programming language",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc003",
		imgLink:"https://www.freelancinggig.com/blog/wp-content/uploads/2018/04/Spring-Framework-Guide.png",
		name: "Java - Springboot",
		description: "A course on this programming language",
		price: 50000,
		onOffer: true
	}
];

export default coursesData;