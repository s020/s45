import {Row, Col, Card, Button} from 'react-bootstrap';
import React, {useState, useRef, useEffect	} from 'react';
import PropTypes from 'prop-types'

export default function CourseCard(courseProp) {
	const {name, imgLink, description, price, onOffer} = courseProp;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [isOpen, setOpen] = useState(true);
	const handleClick = () => {
		if (seats > 0){
			setCount(count + 1);
			setSeats(seats - 1);
		}
	};
	const imgStyle = {
		backgroundImage: `url(${imgLink})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center'
	};

	useEffect(() => {
		setOpen((seats > 0) ? true : false);
	}, [seats]);	

	return(
		<Card className="fitCardY my-3">
			<Card.Body className="d-flex flex-column" style={imgStyle}>
				<Card.Title className="m-0"><h3>{name}</h3></Card.Title>
				<hr className="mb-2 mt-0"/>
				<Card.Text className="m-0 p-0">Learn Fullstack web development and become a certified fullstack developer in just 1 year!</Card.Text>
				<Card.Text className="m-0 p-0">Price: {price}/year</Card.Text>
				<Card.Text className="m-0 p-0">Available Seats: {seats}</Card.Text>
				<Card.Text className="m-0 p-0">Enrollees: {count}</Card.Text>
				{isOpen ? <Button className="align-self-end w-25" onClick={handleClick}>Enroll</Button> : 
				<Button className="align-self-end w-25" disabled>Enroll</Button>}
				
			</Card.Body>
		</Card>
	);
}

CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		id: PropTypes.string.isRequired,
		imgLink: PropTypes.string,
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		onOffer: PropTypes.bool.isRequired
	})
}