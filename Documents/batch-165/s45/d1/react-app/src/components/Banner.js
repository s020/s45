import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

export default function Banner(props) {
	return(
		<Row className="py-5 text-center">
			<Col>
				<h1>Batch 165 Zuitt Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	);
}