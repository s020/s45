import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return(
		<Row className="py-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p=3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Now you can achieve your dream of becoming a programmer and learn to code from home. No programming background is required! What's more is that you can do this all at the comfort of your own homes!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							No enrollment funds? No problem! We have opened our Study Now Pay Later program
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Now you can achieve your dream of becoming a programmer and learn to code from home.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}